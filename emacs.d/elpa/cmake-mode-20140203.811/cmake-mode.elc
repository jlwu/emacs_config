;ELC   
;;; Compiled by alun@Alundediannao.local on Wed Feb  5 00:45:03 2014
;;; from file /Users/alun/.emacs.d/elpa/cmake-mode-20140203.811/cmake-mode.el
;;; in Emacs version 24.3.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#@281 *The name of the cmake executable.

This can be either absolute or looked up in $PATH.  You can also
set the path with these commands:
 (setenv "PATH" (concat (getenv "PATH") ";C:\\Program Files\\CMake 2.8\\bin"))
 (setenv "PATH" (concat (getenv "PATH") ":/usr/local/cmake/bin"))
(custom-declare-variable 'cmake-mode-cmake-executable "cmake" '(#$ . -545) :type 'file :group 'cmake)
(defconst cmake-regex-blank "^[ 	]*$")
(defconst cmake-regex-comment "#.*")
(defconst cmake-regex-paren-left "(")
(defconst cmake-regex-paren-right ")")
(defconst cmake-regex-argument-quoted "\"\\([^\"\\\\]\\|\\\\\\(.\\|\n\\)\\)*\"")
(defconst cmake-regex-argument-unquoted "\\([^ 	\n()#\"\\\\]\\|\\\\.\\)\\([^ 	\n()#\\\\]\\|\\\\.\\)*")
(defconst cmake-regex-token (concat "\\(" cmake-regex-comment #1="\\|" cmake-regex-paren-left #1# cmake-regex-paren-right #1# cmake-regex-argument-unquoted #1# cmake-regex-argument-quoted "\\)"))
(defconst cmake-regex-indented (concat "^\\(" cmake-regex-token "\\|" "[ 	\n]" "\\)*"))
(defconst cmake-regex-block-open "^\\(if\\|macro\\|foreach\\|else\\|elseif\\|while\\|function\\)$")
(defconst cmake-regex-block-close "^[ 	]*\\(endif\\|endforeach\\|endmacro\\|else\\|elseif\\|endwhile\\|endfunction\\)[ 	]*(")
#@69 Determine whether the beginning of the current line is in a string.
(defalias 'cmake-line-starts-inside-string #[nil "\212\301 \210`eb\210\302\303`\"8*\205 \304\207" [parse-end beginning-of-line 3 parse-partial-sexp t] 4 (#$ . 1783)])
#@73 Move to the beginning of the last line that has meaningful indentation.
(defalias 'cmake-find-last-indented-line #[nil "`\304\305y\210\306`	\"o?\2057 \307\n!\204, \310 \204, \311\"\205( G\312\225U?\2057 \305y\210\306`	\"\202\f *\207" [region point-start cmake-regex-blank cmake-regex-indented nil -1 buffer-substring-no-properties looking-at cmake-line-starts-inside-string string-match 0] 3 (#$ . 2027)])
#@36 Indent current line as CMAKE code.
(defalias 'cmake-indent #[nil "\306 ?\205\203 o\203 \307\310!\207\311\212\312 \210`\313\311\314 \210\315 \316\f\313#\203c \317\310!\320\321\322Q	\"\203; \\\320\321\322Q	\"\203K Z\320	\"\203  \323\324P!\203  \\\202  b\210\323!\203r Z,\310W\203 \307\310!\202\202 \307!)\207" [cur-indent token case-fold-search point-start cmake-regex-token cmake-regex-paren-left cmake-line-starts-inside-string cmake-indent-line-to 0 nil beginning-of-line t cmake-find-last-indented-line current-indentation re-search-forward match-string string-match "^" "$" looking-at "[ 	]*" cmake-tab-width cmake-regex-paren-right cmake-regex-block-open cmake-regex-block-close] 4 (#$ . 2446) nil])
(defalias 'cmake-point-in-indendation #[nil "\300\301\302 `{\"\207" [string-match "^[ \\t]*$" point-at-bol] 4])
#@175 Indent the current line to COLUMN.
If point is within the existing indentation it is moved to the end of
the indentation.  Otherwise it retains the same position on the line
(defalias 'cmake-indent-line-to #[(column) "\301 \203	 \302!\207\212\302!)\207" [column cmake-point-in-indendation indent-line-to] 2 (#$ . 3305)])
#@52 Convert all CMake commands to lowercase in buffer.
(defalias 'unscreamify-cmake-buffer #[nil "eb\210\300\301\302\303#\205 \304\305\306!\305\307!\227\305\310!Q\303\"\210\202 \207" [re-search-forward "^\\([ 	]*\\)\\(\\w+\\)\\([ 	]*(\\)" nil t replace-match match-string 1 2 3] 5 (#$ . 3634) nil])
#@42 Highlighting expressions for CMAKE mode.
(defconst cmake-font-lock-keywords (list '("^[ 	]*\\(\\w+\\)[ 	]*(" 1 font-lock-function-name-face)) (#$ . 3937))
#@30 Syntax table for cmake-mode.
(defvar cmake-mode-syntax-table nil (#$ . 4098))
(byte-code "\301\211\207" [cmake-mode-syntax-table nil] 2)
(defvar cmake-mode-hook nil)
(defvar cmake-tab-width 2)
#@41 Major mode for editing CMake listfiles.
(defalias 'cmake-mode #[nil "\306 \210\307\310\311 \312\n!\210\313\314\315\n#\210\313\316\317\n#\210\313\320\321\n#\210\313\322\323\n#\210\313\324\325\n#\210\326\303!\210\327\326\304!\210\330\326\305!\210\331\332\333!\207" [major-mode mode-name cmake-mode-syntax-table font-lock-defaults indent-line-function comment-start kill-all-local-variables cmake-mode "CMAKE" make-syntax-table set-syntax-table modify-syntax-entry 95 "w" 40 "()" 41 ")(" 35 "<" 10 ">" make-local-variable (cmake-font-lock-keywords) cmake-indent "#" run-hooks cmake-mode-hook] 4 (#$ . 4298) nil])
#@122 Runs the command cmake with the arguments specified.  The
optional argument topic will be appended to the argument list.
(defalias 'cmake-command-run #[(type &optional topic buffer) "\203 \202 \306	\n\205 \307\n\310\260\311!\203  \311!\202# \312!\f\313	\313\n\260\314\315\"\210\316 r\317\216\320\321\322\"!\210\323 \210\324\325!.\207" [buffer type topic bufname cmake-mode-cmake-executable command "*CMake" "-" "*" get-buffer generate-new-buffer " " nil shell-command internal--before-save-selected-window ((internal--after-save-selected-window save-selected-window--state)) select-window display-buffer not-this-window cmake-mode toggle-read-only t resize-mini-windows save-selected-window--state] 5 (#$ . 4921) "s"])
#@42 Prints out a list of the cmake commands.
(defalias 'cmake-help-list-commands #[nil "\300\301!\207" [cmake-command-run "--help-command-list"] 2 (#$ . 5667) nil])
#@21 Topic read history.
(defvar cmake-help-command-history nil (#$ . 5834))
#@46 List of available topics for --help-command.
(defvar cmake-help-commands nil (#$ . 5912))
#@88 Run cmake --help-command-list and return a list where each element is a cmake command.
(defalias 'cmake-command-list-as-list #[nil "\302\303 \304\216\305\306\307#\210rq\210\310\311ed\"\312\313#A,\207" [temp-buffer-name #1=#:wconfig "*CMake Commands Temporary*" current-window-configuration ((set-window-configuration #1#)) cmake-command-run "--help-command-list" nil split-string buffer-substring-no-properties "\n" t] 4 (#$ . 6008)])
(require 'thingatpt)
#@86 Gets the topic from the minibuffer input.  The default is the word the cursor is on.
(defalias 'cmake-get-command #[nil "\303 \304\305	\203 	\202 \306 \211\307\310\311&\211\312\230\203$ \313\314!\202% \n*\207" [default-entry cmake-help-commands input word-at-point completing-read "CMake command: " cmake-command-list-as-list nil t cmake-help-command-history "" error "No argument given"] 8 (#$ . 6474)])
#@76 Prints out the help message corresponding to the command the cursor is on.
(defalias 'cmake-help-command #[nil "\300\301\302 \227\303#\207" [cmake-command-run "--help-command" cmake-get-command "*CMake Help*"] 4 (#$ . 6892) nil])
(byte-code "\300\301\302\"\210\300\301\303\"\210\304\305!\207" [add-to-list auto-mode-alist ("CMakeLists\\.txt\\'" . cmake-mode) ("\\.cmake\\'" . cmake-mode) provide cmake-mode] 3)

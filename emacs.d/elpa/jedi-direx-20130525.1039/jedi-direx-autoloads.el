;;; jedi-direx-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (jedi-direx:switch-to-buffer jedi-direx:pop-to-buffer)
;;;;;;  "jedi-direx" "jedi-direx.el" (21231 41623 0 0))
;;; Generated autoloads from jedi-direx.el

(autoload 'jedi-direx:pop-to-buffer "jedi-direx" "\


\(fn)" t nil)

(autoload 'jedi-direx:switch-to-buffer "jedi-direx" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("jedi-direx-pkg.el") (21231 41623 417066
;;;;;;  0))

;;;***

(provide 'jedi-direx-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; jedi-direx-autoloads.el ends here

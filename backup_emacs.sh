#!/bin/bash

## Settings of script.
Project_path=`pwd`
Project_name="Back up emacs"

File_path="~/"
File_list=(.emacs .emacs.d)
Rename=(emacs emacs.d)



#############################################
## Functions
#############################################

echo "Backup configs of emacs 24.*."

for i in 0 1
do
    cp -rf ~/${File_list[$i]} ./${Rename[$i]}

#    echo $File_path${File_list[$i]} ./${Rename[$i]}
done
